package org.oscarehr.PMmodule.caisi_integrator;

import java.io.Serializable;
import java.util.List;

public class ProgramDeleteIdWrapper implements Serializable
{

	private List<Integer> ids;

	public ProgramDeleteIdWrapper()
	{
		ids = null;
	}

	public ProgramDeleteIdWrapper(List<Integer> ids)
	{
		this.ids = ids;
	}

	public List<Integer> getIds()
	{
		return ids;
	}

	public void setIds(List<Integer> ids)
	{
		this.ids = ids;
	}

}
