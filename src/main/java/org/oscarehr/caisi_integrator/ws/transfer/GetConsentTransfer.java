package org.oscarehr.caisi_integrator.ws.transfer;

import java.io.Serializable;
import java.util.Date;

public class GetConsentTransfer implements Serializable
{
	public static enum ConsentState
	{
		ALL, SOME, NONE;
	}

	private Integer integratorFacilityId;
	private ConsentState consentState;
	private Date consentDate;

	public Integer getIntegratorFacilityId()
	{
		return integratorFacilityId;
	}

	public void setIntegratorFacilityId(Integer integratorFacilityId)
	{
		this.integratorFacilityId = integratorFacilityId;
	}

	public ConsentState getConsentState()
	{
		return consentState;
	}

	public void setConsentState(ConsentState consentState)
	{
		this.consentState = consentState;
	}

	public Date getConsentDate()
	{
		return consentDate;
	}

	public void setConsentDate(Date consentDate)
	{
		this.consentDate = consentDate;
	}

}
