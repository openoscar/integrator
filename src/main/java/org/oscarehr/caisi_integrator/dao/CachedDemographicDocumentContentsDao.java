package org.oscarehr.caisi_integrator.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

@Repository
public class CachedDemographicDocumentContentsDao extends AbstractDao<CachedDemographicDocumentContents>
{

	public CachedDemographicDocumentContentsDao()
	{
		super(CachedDemographicDocumentContents.class);
	}

	public CachedDemographicDocumentContents findByFacilityIdAndDemographicId(Integer facilityId, Integer demographicId)
	{
		Query query = entityManager.createNativeQuery("select * from " + modelClass.getSimpleName() + " where integratorFacilityId=?1 and caisiItemId=?2", modelClass);
		query.setParameter(1, facilityId);
		query.setParameter(2, demographicId);

		return getSingleResultOrNull(query);

		//		DataHandler dataHandler = null;
		//
		//		try
		//		{
		//			if (result != null)
		//			{
		//				dataHandler = new DataHandler(new ByteArrayDataSource(result.getBinaryStream(), "application/octet-stream"));
		//			}
		//		}
		//		catch (IOException e)
		//		{
		//			MiscUtils.getLogger().error("Error", e);
		//		}
		//		catch (SQLException e)
		//		{
		//			MiscUtils.getLogger().error("Error", e);
		//		}
		//
		//		CachedDemographicDocumentContents cachedDemographicDocumentContents = new CachedDemographicDocumentContents();
		//		cachedDemographicDocumentContents.setFileContents(dataHandler);
		//		cachedDemographicDocumentContents.getFacilityIntegerCompositePk().setCaisiItemId(demographicId);
		//		cachedDemographicDocumentContents.getFacilityIntegerCompositePk().setIntegratorFacilityId(facilityId);
		//
		//		return cachedDemographicDocumentContents;
	}
}