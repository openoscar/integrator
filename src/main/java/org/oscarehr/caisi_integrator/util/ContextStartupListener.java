package org.oscarehr.caisi_integrator.util;

import org.apache.log4j.Logger;
import org.oscarehr.caisi_integrator.importer.ImportJob;
import org.oscarehr.caisi_integrator.importer.WSUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

public class ContextStartupListener implements javax.servlet.ServletContextListener
{
	private static final Logger logger = MiscUtils.getLogger();

	@Override
	public void contextInitialized(javax.servlet.ServletContextEvent sce)
	{
		String contextPath = sce.getServletContext().getContextPath();

		ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(sce.getServletContext());
		WSUtils.setApplicationContext(applicationContext);

		ImportJob.start();

		logger.info("Server processes starting. context=" + contextPath);

		Long vmstatloggingperiod = Long.parseLong(ConfigXmlUtils.getPropertyString("misc", "vmstat_logging_period"));

		MiscUtils.addLoggingOverrideConfiguration(contextPath);

		if (vmstatloggingperiod > 0)
		{
			VmStat.startContinuousLogging(vmstatloggingperiod);
		}

		if (Boolean.parseBoolean(ConfigXmlUtils.getPropertyString("misc", "enable_homeless_population_reports")))
		{
			HomelessPopulationReportGeneratorTimerTask.startReportGeneration();
		}

		logger.info("Server processes starting completed. context=" + contextPath);
	}

	@Override
	public void contextDestroyed(javax.servlet.ServletContextEvent sce)
	{
		logger.info("Server processes stopping. context=" + sce.getServletContext().getContextPath());

		HomelessPopulationReportGeneratorTimerTask.stopReportGeneration();
		ImportJob.stop();
		VmStat.stopContinuousLogging();

	}
}
